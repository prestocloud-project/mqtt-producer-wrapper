package producer;

import exceptions.NoHostException;
import org.eclipse.paho.client.mqttv3.*;

import javax.net.ssl.SSLHandshakeException;
import java.io.IOException;
import java.util.Iterator;

public class MqttProducer extends RabbitMqProducer {

    //    private Boolean cleanSession = true;
    private MqttConnectOptions conOpt;

    public MqttProducer(String address, boolean isDNS, String topic) {
        super(address, isDNS, topic);
    }

    public MqttProducer(String address, boolean isDNS) {
        super(address, isDNS, null);
    }

//    public Boolean getCleanSession() {
//        return cleanSession;
//    }
//
//    public void setCleanSession(Boolean cleanSession) {
//        this.cleanSession = cleanSession;
//    }

    @Override
    protected void setUpConnection() {
        conOpt = new MqttConnectOptions();

        conOpt.setUserName(vhost + ":" + username);
        conOpt.setPassword(password.toCharArray());

        conOpt.setAutomaticReconnect(false);
        conOpt.setCleanSession(true);
        conOpt.setKeepAliveInterval(60);
        conOpt.setConnectionTimeout(0);

        mqttTrySSL();
    }

    private void mqttTrySSL() {
        protocolPart = "tcp://";
        if (isSsl()) {
            try {
                prepareSSL();
                protocolPart = "ssl://";
                conOpt.setSocketFactory(sslSocketFactory);
            } catch (Exception e) {
                logger.error("[x] Connection without SSL!");
                loggException(e);
            }
        }

        updateAllIps();
    }


    @Override
    protected void updateAllIps() {
        if (dns != null) {
            if (!ipList.isEmpty()) {
                String[] allIps = ipList.stream().map(i -> protocolPart + i).toArray(String[]::new);
                conOpt.setServerURIs(allIps);
            }
        } else if (host != null) {
            conOpt.setServerURIs(new String[]{protocolPart + host});
        }
    }

    private void tearDownConnection(MqttClient client) {
        try {
            if (client != null && client.isConnected()) {
                client.disconnect();
            }
        } catch (MqttException e) {
            logger.error("[x] Couldn't disconnect client!");
            loggException(e);
        }

        try {
            if (client != null)
                client.close();
        } catch (MqttException e) {
            logger.error("[x] Couldn't close client!");
            loggException(e);
        }
    }

    @Override
    public void publish(String payload, String newTopic, boolean persistent, boolean retained) throws NoHostException, IOException {
        if (newTopic == null) {
            String errorMessage = "[x] Topic name can't be null";
            logger.info(errorMessage);
            throw new IOException(errorMessage);
        }

        int qoS = 0;
        if (persistent)
            qoS = 1;

        bufferedMessages.add(new BufferedMessage(payload, newTopic, qoS, retained));
        logger.info("[x] Message is buffered!");

        MqttClient client;
        try {
            if (conOpt.getServerURIs() != null)
                client = new MqttClient(conOpt.getServerURIs()[0], MqttClient.generateClientId());
            else {
                String errorMessage = "[x] No host provided or DNS didn't return any record!";
                logger.error(errorMessage);
                throw new NoHostException(errorMessage);
            }
        } catch (MqttException | IndexOutOfBoundsException e) {
            logger.info("[x] Couldn't create client!");
            loggException(e);
            return;
        }

        try {
            client.connect(conOpt);
        } catch (MqttException e) {
            logger.info("[x] Couldn't establish connection!");
            if (e.getCause() instanceof SSLHandshakeException && e.getCause().getMessage().contains("certificate_expired")) {
                mqttTrySSL();
                try {
                    client.connect(conOpt);
                } catch (MqttException e1) {
                    loggException(e1);
                    tearDownConnection(client);
                    return;
                }
            } else {
                loggException(e);
                tearDownConnection(client);
                return;
            }
        }

        try {
            synchronized (bufferedMessages) {
                Iterator<BufferedMessage> iterator = bufferedMessages.iterator();
                while (iterator.hasNext()) {
                    BufferedMessage bufferedMessage = iterator.next();
                    publishMessage(client, bufferedMessage);
                    logger.info("[x] Published \"{}\" message {} on {}.\n", bufferedMessage.getTopic(), bufferedMessage.getPayload(), client.getCurrentServerURI());
                    iterator.remove();
                }
            }
        } catch (MqttException e) {
            logger.info("[x] Some messages were not sent, yet!");
            loggException(e);
        } finally {
            tearDownConnection(client);
        }
    }

    private void publishMessage(MqttClient client, BufferedMessage bufferedMessage) throws MqttException {
        MqttMessage message = new MqttMessage(bufferedMessage.getPayload().getBytes());
        message.setQos(bufferedMessage.getQos());
        message.setRetained(bufferedMessage.isRetained());
        MqttTopic topic = client.getTopic(bufferedMessage.getTopic());
        MqttDeliveryToken token = topic.publish(message);
        token.waitForCompletion();
    }

//    public static void main(String[] args) {
//        MqttProducer producer = new MqttProducer("3.120.91.124", true, "test.1.2.3.4.5");
//        producer.useSSL("3.120.91.124", "s.pV8wvr4dkuq5ii476ddErT07");
//
//        for (int i = 1; ; i++)
//            try {
//                producer.publish("test" + i);
//                Thread.sleep(5000);
//            } catch (IOException | InterruptedException | NoHostException e) {
//                e.printStackTrace();
//            }
//    }
}
