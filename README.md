# MQTT MESSAGE PRODUCER WRAPPER

Mqtt producer wrapper is a library that uses [`eclipse.paho.client.mqttv3` version `1.2.2`](https://mvnrepository.com/artifact/org.eclipse.paho/org.eclipse.paho.client.mqttv3/1.2.2) and adds a layer for easier connectivity with the PrEsto Communication and Messaging Broker.

Libraries are available on Nissatech public Maven, and in order to use them you can:

- #### Add the repository and dependency tag to your pom.xml file:
```xml
<repository>
    <id>archiva.public</id>
    <name>Nissatech Public Repository</name>
    <url>https://maven.nissatech.com/repository/public/</url>
    <releases>
        <enabled>true</enabled>
    </releases>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</repository>
```
```xml
<dependency>
    <groupId>com.nissatech.presto</groupId>
    <artifactId>mqtt-producer</artifactId>
    <version>7.0</version>
</dependency>
```
- #### Download jar and include it in your project:

https://maven.nissatech.com/repository/public/com/nissatech/presto/mqtt-producer/7.0/mqtt-producer-7.0-jar-with-dependencies.jar

## Basic usage
```java
import producer.MqttProducer;
import exceptions.NoHostException;
import java.io.IOException;

public class Send_MQTT {
    public static void main(String argv[]) {
        String topic = "mqtt/sending/test";
        MqttProducer producer = new MqttProducer("3.120.91.124", true, topic);
        //producer.setUsernameAndPassword("nissatech","**********");
        //producer.useSSL("3.120.91.124", "**********");
        int i = 0;
        try {
            while (true) {
                String Object = "Hello world!";
                producer.publish(Object);
                Thread.sleep(10000);
            }
        } catch (InterruptedException | NoHostException | IOException e1) {
            e1.printStackTrace();
        }
    }
}
```
## Constructors

`MqttProducer(String host_or_dns_address, boolean is_DNS_address, String topic)` - creates instance of MqttProducer

| Param | Description | Required |
|---|:---:|---|
| host_or_dns_address | IP address of broker node or of the load balancing DNS | yes |
| is_DNS_address | is the first field DNS address or not | yes |
| topic | topic name to which message should be sent to | no |

> Note: There are two constructors. One with all 3 arguments and other with only first 2, so when publishing message you need to use method with newTopic argument.

By default:
- [Automatic reconnect](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#isAutomaticReconnect--) is set to false, because messages are buffered. Also, connection
  timeout (connection establishment timeout in milliseconds; zero for infinite) is set to 0.
- The [clean session](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#isCleanSession--) is set to true, the client is not subscribing, but only publishing messages to
  topics, it doesn't need for the server to treat a subscription as durable (to any session
  information be stored on the broker).
- [Keep-alive](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html#setKeepAliveInterval-int-) interval (measured in seconds) defines the maximum time interval between
  messages sent or received. It enables the client to detect if the server is no longer available,
  without having to wait for the TCP/IP timeout. The client will ensure that at least one
  message travels across the network within each keep-alive period. In the absence of a
  data-related message during the time period, the client sends a very small "ping" message,
  which the server will acknowledge. The default value is 60 seconds.
- Port is automatically set to 1883. When the user enables SSL/TLS on the initiation of a
  connection, the port is changed to 8883.
- Exchange name is set in the rabbitmq config file on the broker node
  - {exchange, <<"presto.cloud">>}
  
## Setters

`void setUsernameAndPassword(String username, String password)` - set username and password for connecting to the broker

`void setVhost(String vhost)` - set [virtual host](https://www.rabbitmq.com/vhosts.html) for connecting to the broker

`void useSSL(String vault, String token)` - set vault address and token for SSL/TLS connection
- sslSocketFactory is prepared with the certificate
  received from the Vault server (TLS v1.2) and added to the [connection options](https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html). If it failed to create this
  factory it logs a warning and set connection without SSL/TLS.

`void turnOnSsl(boolean ssl)` - turn on/off usage of SSL/TLS certificates

`void setPersistent(boolean persistent)` - turn on/off persistence of the messages

`void setBufferedMessages(List<BufferedMessage> bufferedMessages)` - add messages to the buffer (used to restore state after producer app goes down)

## Getters

`String getUsername()` - get current username

`String getPassword()` - get current password
- for now, it uses the default (guest) username and password.

`String getVhost()` - get current virtual host
- default vhost is "/", and probably there is no need for some other, but we enable the user to
  set vhost if later we decide to create specific vhost for specific users.
  
`boolean isSsl()` - check if connection is secure

`boolean arePersistent()` - check if all messages are persistent

`List<BufferedMessage> getBufferedMessages()` - get all buffered messages
-  user can save all buffered messages to file if the producer app goes down

## Publish methods

`void publish(String payload, String newTopic, boolean persistent, boolean retained)` - publish message to the broker

| Param | Description | Required | Default
|---|:---:|---|---|
| payload | body of message to be sent | yes | / |
| newTopic | topic name on which message should be sent | no | topic set in constructor |
| persistent | QOS of message | no | true |
| retained | should message be retained on broker | no | false |

> Note: There are 4 versions of this method with a different number of arguments

Publish method checks if the newTopic is not null, buffers message, tries to create client, connects the client to the
broker with set options, renews the certificate if needed, publishes message to a defined topic, logs work and closes client:
- Check if the topic name that user tries to connect with isn't null. Log error message and return
IOException.
- Persistence of message is automatically set to true. With one of the publish functions, the
user can set persistent and/or retained status of the current message. Or it can set
persistence for all messages with setPersistent. True set QoS to 1, and false is setting it to 0.
By default, the message is not retained.
- Buffer message and log work.
- After this try to create new MQTT client with random generated ID and broker URL. If it
doesn't succeed log the message and error.
- Connect to broker with specified connection options. If the connection fails because of an
expired certificate, the certificate is renewed and new connection created. If it fails for some
other reason, client will be closed, the error will be logged and because messages are buffered we don't
need to send the exception to the user.
- On the first successful connection, publish all buffered messages like FIFO, and log work.
- In the end, disconnect and close the client.

